# Installation #

## Define components ##

$ cat components

`panel
bind
health-monitor
fail2ban
l10n
pmm
mysqlgroup
roundcube
drweb
spamassassin
selinux
postfix
dovecot
proftpd
awstats
modsecurity
mod_fcgid
webservers
php7.1
php5.6
phpgroup
nginx
config-troubleshooter
psa-firewall
psa-vpn
psa-fileserver
watchdog
cloudflare
magicspam
heavy-metal-skin
wp-toolkit
security-advisor
letsencrypt`

## Install ##

`$ cat install_smb.sh

[root@plesk-onyx installer]# cat install_smb.sh
#!/bin/bash

setenforce 0

wget https://autoinstall.plesk.com/plesk-installer

chmod +x plesk-installer

install="./plesk-installer --select-release-latest "

for i in `cat components`;
        do install=$install" --install-component $i";
done
echo $install >> install.log
bash $install >> install.log`


# Image customization #

Now it is good time to login and apply custom branding colors, logo and promos like from [Promos guide](https://docs.plesk.com/en-US/onyx/advanced-administration-guide-linux/customizing-plesk-appearance-and-gui-elements/hiding-and-changing-plesk-gui-elements/promos/creating-a-promo.68603/)

# Cloning preparation #

Prepare image for cloning as per [cloning guide](https://docs.plesk.com/en-US/onyx/deployment-guide/plesk-installation-and-upgrade-on-multiple-servers/deploying-plesk-servers-by-cloning/preparing-plesk-for-linux-for-cloning.76921/)
Server is ready to be cloned and handed over to client.

# First run #

Add some sugar: free customer of extra clicks and inputs, do this on his behalf.

## Initialize in a way customer does not need to agree to license or click "OK", just get to the panel directly. Open firewall on 8443 ##

`$ cat init.sh
#!/bin/bash
/usr/sbin/plesk bin init_conf --init -default-ip {$ip} -netmask 255.255.255.0 -iface eth0 -ip-type shared -hostname {$hostname} -name {$name} -passwd {$plesk_password} -email {$email} -company {$company} -license_agreed true -admin_info_not_required true
/usr/sbin/plesk bin poweruser --off 
iptables -I INPUT -p tcp --dport 8443 -j ACCEPT`

## Install required extensions. Pay attention Kolab is taken from internal repo, chage after publishing ##

### Define extensions set ###

`[root@plesk-onyx httpdocs]# cat extensions
https://ext.plesk.com/packages/bbc5272e-7b88-4d5a-8a50-67431075fac4-spamexperts-extension/download?1.1-0
https://ext.plesk.com/packages/b49f9b1b-e8cf-41e1-bd59-4509d92891f7-magicspam/download?2.0.7-1
https://ext.plesk.com/packages/f6847e61-33a7-4104-8dc9-d26a0183a8dd-letsencrypt/download?2.0.3-31
https://ext.plesk.com/packages/585327df-b695-4b8f-9ade-2e9d1962d4c2-plesk-mobile/download?1.7-5
https://ext.plesk.com/packages/24e15804-3134-4561-a2a2-1bdc7228e98f-website-virus-check/download?1.2-1
https://ext.plesk.com/packages/e757450e-40a5-44e5-a35d-8c4c50671019-dgri/download?2.1-0
http://extore.pp.plesk.ru/packages/ef40e242-5927-4e75-ae43-d12940c92695-kolab/download?16.1-19`

### Install extensions ###

`[root@plesk-onyx httpdocs]# cat install_extensions.sh
#!/bin/bash

for i in `cat extensions`;
do
        /usr/sbin/plesk bin extension --install-url $i;
done`


# Licensing #

## Generate license keys, including for extensions.  ##
### Define SKUs. NOTE: few SKU are not yet available ###
`[root@plesk-onyx httpdocs]# cat licenses
PLESK-12-WEB-PRO-1M
ADD-DATAGRID-SCANNER-1M
ADD-KASPERSKY-ANTIVIRUS-1M
ADD-MAGICSPAM-1M
ADD-SPAMEXPERTS-INOUT-1-DOM-1M`

### Generate keys ###

`[root@plesk-onyx installer]# cat generate_keys.sh
#!/bin/sh

for i in `cat licenses`;

do

echo $i;

echo > payload
echo \{ >> payload
echo \"ownerId\"\ :\ 701737563\,>>payload
echo  \"items\"\ :\ \[ >>payload
echo     \{>>payload
echo       \"item\"\ :\ \"$i\">>payload
echo     \}>>payload
echo   \]>>payload
echo \}\;>>payload;

curl -v -Li -X POST \
--user "USERNAME:PASSWORD" \
-H "Content-Type: application/json; charset=UTF-8" \
 "https://ka.demo.plesk.com:7050/jsonrest/business-partner/30/keys?return-key-state=true" \
-d@payload >> activation.txt

done

grep -i Code activation.txt |awk -F\" '{print $16}' >>activationcodes.txt`

### Install license keys ###

`[root@plesk-onyx installer]# cat install_keys.sh
#!/bin/bash

for i in `head -1 activationcodes.txt`;
do
/usr/sbin/plesk bin license -i $i;
done

for j in `tail -n +2 activationcodes.txt`;
do
/usr/sbin/plesk bin license -i $j -additional-key true;
done`


# Post-install #
## Security: fail2ban and firewall ##
### Define jails for fail2ban ###

`[root@plesk-onyx installer]# cat jails
ssh
recidive
plesk-proftpd
plesk-postfix
plesk-dovecot
plesk-roundcube
plesk-apache
plesk-apache-badbot
plesk-panel
plesk-modsecurity
plesk-wordpress

### Enable fail2ban ###

[root@plesk-onyx installer]# cat security.sh
#!/bin/bash

/usr/sbin/plesk bin ip_ban --enable

for i in `cat jails`;
do
/usr/sbin/plesk bin ip_ban --enable-jails $i;
done`

### Enable firewall management from Plesk panel ###

**TODO** - didnt find an easy way to emulate that button click